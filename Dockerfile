FROM openjdk:8-jdk-alpine
ADD target/*.jar microservicepaiement.jar
ENTRYPOINT ["java","-jar","/microservicepaiement.jar"]
